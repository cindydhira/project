<?php
	session_start();
	
	include("../lib/koneksi.php");
	define("INDEX", true);
?>

<html>
	<head>
		<title>Halaman Admin</title>

			<link rel="stylesheet" href="../css/admin.css">
</head>
<body>
<div id="container">

	<div id="header"> </div>

	<div id="menu">
		<?php include("menu.php"); ?>
	<div>

	<div id="content">
		<?php include("konten.php"); ?>
	</div>
	
	<div id="footer">
		<p> Copyright &copy; DhiraCindy</p>
	</div>
</div>
</body>
</html?


